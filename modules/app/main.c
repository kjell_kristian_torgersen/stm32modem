#include <stdio.h>
#include <math.h>
#include <string.h>

#include "main.h"

extern void modem_write(const void * buf, int count);

extern ADC_HandleTypeDef hadc1;
extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim4;
extern UART_HandleTypeDef huart1;

#define SAMPLES 8

static uint32_t timer=0;
static int16_t ringbuf[SAMPLES]={0};
static int32_t accbuf[SAMPLES]={0};
static int32_t acc = 0;
static int bufidx = 0;

static int DECODER_state;
static int THRESHOLD = 125903948/1000;
static int DECODER_ctr = 0;
static int DECODER_rxword=0;
static int DECODER_rxbits=0;
int DECODER_rxchar = -1;

#define NDUMP 64*20
int32_t dumpidx = 0;
static int32_t dump[NDUMP];


void app_main_setup(void)
{
	HAL_TIM_Base_Start(&htim3);
	HAL_TIM_PWM_Start_IT(&htim4, TIM_CHANNEL_1);
	//HAL_TIM_PWM_Start_IT(&htim4, TIM_CHANNEL_2);
	//HAL_TIM_OC_Start(&htim4, TIM_CHANNEL_4);
	HAL_ADC_Start_IT(&hadc1);
}

volatile int32_t min = 0;
volatile int32_t max = 0;

static void MODEM_Decoder(int32_t bit)
{
	//HAL_UART_Transmit(&huart1, &bit, 4, 10);
	if(bit < min) min = bit;
	if(bit > max) max = bit;
	switch(DECODER_state) {
		case 0: // Vent p� startbit
		if(bit < -THRESHOLD) { // Startbit passerer terskel, hopp til neste tilstand for  validere
			DECODER_state = 1;
			DECODER_ctr = 1;
		}
		break;
		case 1: // Validering av startbit, sjekk at vi har halve startbitet i nrheten av 0 eller lavere.
		DECODER_ctr++;
		if(DECODER_ctr >= SAMPLES/2) { // Start bit OK. Hopp til neste tilstand for sample databit
			DECODER_rxbits = 0; // Nullstill variabler
			DECODER_ctr = 1;
			DECODER_rxword = 0;
			DECODER_state = 2;
		}
		if(bit > THRESHOLD) { // Start bitet var ikke godt nok. Hopp tilbake til tilstanden for  vente p nytt startbit
			DECODER_state = 0;
		}
		break;
		case 2: // Samle databittene
		DECODER_ctr++; // Tell opp en symbol periode. N startet vi midt i forrige bit,  vil ende i midten av neste bit
		if(DECODER_ctr >= SAMPLES) {
			DECODER_ctr = 0; // Nullstill teller  lagre unna mottatt bit
			DECODER_rxword >>= 1; // Lag plass til neste bit
			DECODER_rxword |= (bit<0) ? 0 : 0x200; // Legg til bit dersom 1
			DECODER_rxbits++; // Tell antall mottatte bit
			if(DECODER_rxbits >= 9) { // Dersom 9 bit, valider stoppbit
				if(DECODER_rxword & 0x200) { // stoppbit er ok
					DECODER_rxword >>= 1; // fjern stoppbit
					DECODER_rxchar = DECODER_rxword; // kopier ut mottatt karakter
					DECODER_state = 0; // ferdig, vent p neste startbit
					} else {
					// ikke gyldig stoppbit,
					DECODER_state = 0; // ferdig, vent p neste startbit
				}
			}
		}
		break;
		default:
		DECODER_state = 0;
		break;
	}
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_3, GPIO_PIN_SET);
	int32_t newest = HAL_ADC_GetValue(hadc) - 2048;
	int32_t oldest = ringbuf[bufidx];
	ringbuf[bufidx] = newest;

	int32_t prod = newest * oldest;

	acc -= accbuf[bufidx & (SAMPLES/2 - 1)];
	accbuf[bufidx & (SAMPLES/2 - 1)] = prod;
	acc += prod;
	bufidx++;
	if(bufidx >= SAMPLES) {
		bufidx = 0;
	}

	if(dumpidx < NDUMP) {
		dump[dumpidx] = acc;
		dumpidx++;
	}
	//htim4.Instance->CCR2 = 1874/2  + (acc >> 16);

	MODEM_Decoder(acc);

	/*if(acc < 0) {

	} else {

	}*/
	//HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_3);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_3, GPIO_PIN_RESET);
}

int nr = 0;
void app_main_loop(void)
{
	//HAL_TIM_PWM_ConfigChannel(&htim4, , Channel)
	//uint16_t u = (uint16_t)(468.0f+468.0f*sinf(2.0f*M_PI*0.1f*HAL_GetTick()/1000.0f));
	char buf[256];
	//if(HAL_GetTick() - timer >= 1000) {
	if(!modem_busy()) {
		//timer = HAL_GetTick();
		snprintf(buf, sizeof(buf), "Hello %li from modem!\r\n ", nr++);
		modem_write(buf, strlen(buf));
	}
	/*if(dumpidx == NDUMP) {
		for(int i = 0; i < NDUMP; i++) {
			snprintf(buf, sizeof(buf), "%li\r\n", dump[i]);
			HAL_UART_Transmit(&huart1, buf, strlen(buf), 10);
		}
		dumpidx = 0;
	}*/
	if(DECODER_rxchar != -1) {
		HAL_UART_Transmit(&huart1, &DECODER_rxchar, 1, 10);
		DECODER_rxchar = -1;
	}
}
